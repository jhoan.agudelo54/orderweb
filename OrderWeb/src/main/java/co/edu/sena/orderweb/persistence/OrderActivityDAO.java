/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.OrderActivity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class OrderActivityDAO implements IOrderActivityDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(OrderActivity orderActivity) throws Exception {
        try {
           entityManager.persist(orderActivity);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(OrderActivity orderActivity) throws Exception {
        try {
           entityManager.merge(orderActivity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(OrderActivity orderActivity) throws Exception {
         try {
           entityManager.remove(orderActivity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public OrderActivity findById(Integer id_order,Integer id_activity ) throws Exception {
        
       try {
           Query query = entityManager.createNamedQuery("SELECT o from OrderActivity o where o.orderActivityPK.idorder = :id_order and o.orderActivityPK.idActivity").
                   setParameter("idOrder", id_order)
                   .setParameter("idActivity", id_activity);
            return (OrderActivity) query.getSingleResult();
        }
        catch(NoResultException e){
            return null;
        }
        catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<OrderActivity> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("OrderActivity.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}

