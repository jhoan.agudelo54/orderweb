/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.TypeActivityBeanLocal;
import co.edu.sena.orderweb.model.TypeActivity;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author usuario
 */
@Named(value = "typeActivityView")
@RequestScoped
public class TypeActivityView {
    
     private InputText txtIdTypeActivity;
    private InputText txtDescription;
    
    private CommandButton btnModificar;
    private CommandButton btnCrear;
    private CommandButton btnEliminar;
    
    private List<TypeActivity> listTypeActivity = null;
    
    @EJB
    private TypeActivityBeanLocal typeActivityBean;
    /**
     * Creates a new instance of TypeActivityView
     */
    public TypeActivityView() {
    }

    public InputText getTxtIdTypeActivity() {
        return txtIdTypeActivity;
    }

    public void setTxtIdTypeActivity(InputText txtIdTypeActivity) {
        this.txtIdTypeActivity = txtIdTypeActivity;
    }

    public InputText getTxtDescription() {
        return txtDescription;
    }

    public void setTxtDescription(InputText txtDescription) {
        this.txtDescription = txtDescription;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<TypeActivity> getListTypeActivity() {
        if (listTypeActivity == null) {
            try {
                listTypeActivity = typeActivityBean.findAll();
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
        return listTypeActivity;
    }

    public void setListTypeActivity(List<TypeActivity> listTypeActivity) {
        this.listTypeActivity = listTypeActivity;
    }
    public void clear() {

        txtIdTypeActivity.setValue("");
        txtDescription.setValue("");        

        listTypeActivity = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            TypeActivity typeActivity = new TypeActivity();
    
            typeActivity.setDescription(txtDescription.getValue().toString());
            
            typeActivityBean.insert(typeActivity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "TypeActivity creado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void update() {
        try {
            TypeActivity typeActivity = new TypeActivity();

            typeActivity.setIdType(Integer.parseInt(txtIdTypeActivity.getValue().toString()));
            typeActivity.setDescription(txtDescription.getValue().toString());
            
            typeActivityBean.update(typeActivity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "TypeActivity modificado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void delete() {
        try {
            TypeActivity typeActivity = new TypeActivity();
            typeActivity.setIdType(Integer.parseInt(txtIdTypeActivity.getValue().toString()));
            typeActivityBean.delete(typeActivity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "TypeActivity eliminado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void onRowSelect(SelectEvent event) {

        TypeActivity typeActivity = (TypeActivity) event.getObject();
        txtIdTypeActivity.setValue(typeActivity.getIdType());
        txtDescription.setValue(typeActivity.getDescription());                
        
        //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            TypeActivity typeActivity = typeActivityBean.findById(Integer.parseInt(txtIdTypeActivity.getValue().toString()));

            if (typeActivity != null) {
                
                txtDescription.setValue(typeActivity.getDescription());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                txtDescription.setValue("");
                
                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "TypeActivity no registrado"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    
}
