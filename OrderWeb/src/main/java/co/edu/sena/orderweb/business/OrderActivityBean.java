/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.OrderActivity;
import co.edu.sena.orderweb.persistence.IOrderActivityDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class OrderActivityBean implements OrderActivityBeanLocal {

    @EJB
    private IOrderActivityDAO orderActivityDAO;

    public void validate(OrderActivity orderActivity) throws Exception {
        if (orderActivity == null) {
            throw new Exception("El tipo de actividad es nula");
        }
        if (orderActivity.getOrder1() == null) {
            throw new Exception("El id es obligatorio");
        }
        if (orderActivity.getOrderActivityPK() == null) {
            throw new Exception("La actividad es obligatoria");
        }
        if (orderActivity.getSerial() == 0) {
            throw new Exception("El serial es obligatoria");
        }
    }

    @Override
    public void insert(OrderActivity orderActivity) throws Exception {
        validate(orderActivity);
        OrderActivity oldOrderActivity = orderActivityDAO.findById(Integer.parseInt(orderActivity.getOrder1().toString()),Integer.parseInt( orderActivity.getOrderActivityPK().toString()));
        if (oldOrderActivity != null) {
            throw new Exception("Ya existe un tipo de actividades con el mismo ID");
        }

        orderActivityDAO.insert(orderActivity);
    }

    @Override
    public void update(OrderActivity orderActivity) throws Exception {
        validate(orderActivity);
        OrderActivity oldOrderActivity = orderActivityDAO.findById(Integer.parseInt(orderActivity.getOrder1().toString()),Integer.parseInt( orderActivity.getOrderActivityPK().toString()));
        if (oldOrderActivity == null) {
            throw new Exception("No existe un tipo de actividades con el mismo ID");
        }
        oldOrderActivity.setSerial(orderActivity.getSerial());
        orderActivityDAO.update(orderActivity);
    }

    @Override
    public void delete(OrderActivity orderActivity) throws Exception {
        validate(orderActivity);
        OrderActivity oldOrderActivity = orderActivityDAO.findById(Integer.parseInt(orderActivity.getOrder1().toString()),Integer.parseInt( orderActivity.getOrderActivityPK().toString()));
        if (oldOrderActivity == null) {
            throw new Exception("No existe un tipo de actividades con el mismo ID");
        }
        orderActivityDAO.delete(orderActivity);
    }

    @Override
    public OrderActivity findById(Integer id_order, Integer id_activity) throws Exception {
         if (id_order ==0) {
            throw new Exception("El id es obligatorio");
        }
         if (id_activity ==0) {
            throw new Exception("El id es obligatorio");
        }
        return orderActivityDAO.findById(id_order,id_activity);
    }

    @Override
    public List<OrderActivity> findAll() throws Exception {
         return orderActivityDAO.findAll();
    }

}
