/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Observation;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jsam4
 */
@Local
public interface ObservationBeanLocal {
     public void insert(Observation observation) throws Exception;
    public void update(Observation observation) throws Exception;
    public void delete(Observation observation) throws Exception;
    public Observation findById(Integer id_observation) throws Exception;
    public List<Observation> findAll() throws Exception;
}
