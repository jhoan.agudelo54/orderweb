/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jsam4
 */
@Embeddable
public class OrderActivityPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_order")
    private int idOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_activity")
    private int idActivity;

    public OrderActivityPK() {
    }

    public OrderActivityPK(int idOrder, int idActivity) {
        this.idOrder = idOrder;
        this.idActivity = idActivity;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(int idActivity) {
        this.idActivity = idActivity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idOrder;
        hash += (int) idActivity;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderActivityPK)) {
            return false;
        }
        OrderActivityPK other = (OrderActivityPK) object;
        if (this.idOrder != other.idOrder) {
            return false;
        }
        if (this.idActivity != other.idActivity) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.sena.orderweb.model.OrderActivityPK[ idOrder=" + idOrder + ", idActivity=" + idActivity + " ]";
    }
    
}
