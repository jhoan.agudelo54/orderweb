/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.CausalBeanLocal;
import co.edu.sena.orderweb.business.ObservationBeanLocal;
import co.edu.sena.orderweb.business.OrderBeanLocal;
import co.edu.sena.orderweb.model.Causal;
import co.edu.sena.orderweb.model.Observation;
import co.edu.sena.orderweb.model.Order1;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author usuario
 */
@Named(value = "orderView")
@RequestScoped
public class OrderView {
    
    private InputText txtIdOrder;
    private Date LegalizationDate;
    private InputText txtAddress;
    private InputText txtCity;
    private InputText txtState;
    
    //Fk selection
    private SelectOneMenu selectIdObservation;
    private SelectOneMenu selectIdCausal;
    
    private List<Order1> listOrder = null;
    
    private CommandButton btnModificar;
    private CommandButton btnCrear;
    private CommandButton btnEliminar;

    private List<SelectItem> itemsObservation;
    private List<SelectItem> itemsCausal;
    
    @EJB
    private OrderBeanLocal orderBean;
    @EJB
    private ObservationBeanLocal observationBean;
    @EJB
    private CausalBeanLocal causalBean;
    
    
    
    /**
     * Creates a new instance of OrderView
     */
    public OrderView() {
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }
    
    public InputText getTxtIdOrder() {
        return txtIdOrder;
    }

    public void setTxtIdOrder(InputText txtIdOrder) {
        this.txtIdOrder = txtIdOrder;
    }

    public Date getLegalizationDate() {
        return LegalizationDate;
    }

    public void setLegalizationDate(Date LegalizationDate) {
        this.LegalizationDate = LegalizationDate;
    }

    public InputText getTxtAddress() {
        return txtAddress;
    }

    public void setTxtAddress(InputText txtAddress) {
        this.txtAddress = txtAddress;
    }

    public InputText getTxtCity() {
        return txtCity;
    }

    public void setTxtCity(InputText txtCity) {
        this.txtCity = txtCity;
    }

    public InputText getTxtState() {
        return txtState;
    }

    public void setTxtState(InputText txtState) {
        this.txtState = txtState;
    }

    public SelectOneMenu getSelectIdObservation() {
        return selectIdObservation;
    }

    public void setSelectIdObservation(SelectOneMenu selectIdObservation) {
        this.selectIdObservation = selectIdObservation;
    }

    public SelectOneMenu getSelectIdCausal() {
        return selectIdCausal;
    }

    public void setSelectIdCausal(SelectOneMenu selectIdCausal) {
        this.selectIdCausal = selectIdCausal;
    }

    public List<Order1> getListOrder() {
        if (listOrder == null) {
            try {
                listOrder = orderBean.findAll();
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
        return listOrder;
    }

    public void setListOrder(List<Order1> listOrder) {
        this.listOrder = listOrder;
    }

    public List<SelectItem> getItemsObservation() {
        try {
            List<Observation> listObservation = observationBean.findAll();
            itemsObservation = new ArrayList<>();
            for (Observation observation : listObservation) {
                itemsObservation.add(new SelectItem(observation.getIdObservation()));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
        return itemsObservation;
    }

    public void setItemsObservation(List<SelectItem> itemsObservation) {
        this.itemsObservation = itemsObservation;
    }

    public List<SelectItem> getItemsCausal() {
        try {
            List<Causal> listCausal = causalBean.findAll();
            itemsCausal = new ArrayList<>();
            for (Causal causal : listCausal) {
                itemsCausal.add(new SelectItem(causal.getIdCausal()));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error",e.getMessage()));
        }
        return itemsCausal;
    }

    public void setItemsCausal(List<SelectItem> itemsCausal) {
        this.itemsCausal = itemsCausal;
    }
    public void clear() {

        txtIdOrder.setValue("");
        LegalizationDate = null;
        txtAddress.setValue("");
        txtCity.setValue("");
        txtState.setValue("");

        selectIdCausal.setValue("");
        selectIdObservation.setValue("");

        listOrder = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            Order1 order1 = new Order1();

            order1.setIdOrder(Integer.parseInt(txtIdOrder.getValue().toString()));
            order1.setLegalizationDate(LegalizationDate);
            order1.setAddress(txtAddress.getValue().toString());
            order1.setCity(txtCity.getValue().toString());
            order1.setState(txtState.getValue().toString());
            

            //FK Causal
            Causal causal = causalBean.findById(Integer.parseInt(selectIdCausal.getValue().toString()));
            order1.setIdCausal(causal);

            //FK Observation
            Observation observation = observationBean.findById(Integer.parseInt(selectIdObservation.getValue().toString()));
            order1.setIdObservation(observation);
            
            orderBean.insert(order1);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Order creado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void update() {
        try {
            Order1 order1 = new Order1();

            order1.setIdOrder(Integer.parseInt(txtIdOrder.getValue().toString()));
            order1.setLegalizationDate(LegalizationDate);
            order1.setAddress(txtAddress.getValue().toString());
            order1.setCity(txtCity.getValue().toString());
            order1.setState(txtState.getValue().toString());
            

            //FK Causal
            Causal causal = causalBean.findById(Integer.parseInt(selectIdCausal.getValue().toString()));
            order1.setIdCausal(causal);

            //FK Observation
            Observation observation = observationBean.findById(Integer.parseInt(selectIdObservation.getValue().toString()));
            order1.setIdObservation(observation);
            
            orderBean.insert(order1);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Order modificado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void delete() {
        try {
            Order1 order1 = new Order1();
            order1.setIdOrder(Integer.parseInt(txtIdOrder.getValue().toString()));
            orderBean.delete(order1);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Order eliminado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void onRowSelect(SelectEvent event) {

        Order1 order1 = (Order1) event.getObject();
        LegalizationDate = order1.getLegalizationDate();
        txtIdOrder.setValue(order1.getIdOrder());
        txtAddress.setValue(order1.getAddress());
        txtCity.setValue(order1.getCity());
        txtState.setValue(order1.getState());

        //FK
        selectIdCausal.setValue(order1.getIdCausal().getIdCausal());
        selectIdObservation.setValue(order1.getIdObservation().getIdObservation());

        //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            Order1 order1 = orderBean.findById(Integer.parseInt(txtIdOrder.getValue().toString()));

            if (order1 != null) {
                order1.getLegalizationDate();
                txtAddress.setValue(order1.getAddress());
                txtCity.setValue(order1.getCity());
                txtState.setValue(order1.getState());
                   

                //FK
                selectIdCausal.setValue(order1.getIdCausal().getIdCausal());
                selectIdObservation.setValue(order1.getIdObservation().getIdObservation());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                                
                LegalizationDate =null;  
                txtAddress.setValue("");
                txtCity.setValue("");
                txtState.setValue("");
                
                listOrder = null;    
                
                selectIdCausal.setValue("");
                selectIdObservation.setValue("");                

                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Order no registrado"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    
}
