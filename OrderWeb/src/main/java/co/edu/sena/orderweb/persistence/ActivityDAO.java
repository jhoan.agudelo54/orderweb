/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Activity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ActivityDAO implements IActivityDAO{
    
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Activity activity) throws Exception {
        try {
           entityManager.persist(activity);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Activity activity) throws Exception {
        try {
           entityManager.merge(activity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Activity activity) throws Exception {
         try {
           entityManager.remove(activity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Activity findById(Integer id_activity ) throws Exception {
        
        try {
           return entityManager.find(Activity.class, id_activity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Activity> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Activity.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
