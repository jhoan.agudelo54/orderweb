/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Causal;
import co.edu.sena.orderweb.persistence.ICausalDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class CausalBean implements CausalBeanLocal{
    
     @EJB
    private ICausalDAO causalDAO;

     public void validate(Causal causal) throws Exception{
        if(causal ==null){
            throw new Exception("La causal es nula");
        }
        if (causal.getIdCausal()==0){
            throw new Exception("El id es obligatorio");
        }
        if(causal.getDescription().isEmpty()){
          throw new Exception("La descripcion es obligatoria");
        
        }
    }
     
    @Override
    public void insert(Causal causal) throws Exception {
         validate(causal);
        Causal oldCausal = causalDAO.findById(causal.getIdCausal());
        if(oldCausal != null){
            throw new Exception("Ya existe una causal con el mismo ID");
        }
        
        causalDAO.insert(causal);
    }

    @Override
    public void update(Causal causal) throws Exception {
         validate(causal);
        Causal oldCausal = causalDAO.findById(causal.getIdCausal());
        if(oldCausal == null){
            throw new Exception("No existe una causal con el mismo ID");
        }
        oldCausal.setDescription(causal.getDescription());
        causalDAO.update(causal);
    }

    @Override
    public void delete(Causal causal) throws Exception {
        validate(causal);
        Causal oldCausal = causalDAO.findById(causal.getIdCausal());
        if(oldCausal == null){
            throw new Exception("No existe una causal con el mismo ID");
        }
        
        causalDAO.delete(oldCausal);
    }

    @Override
    public Causal findById(Integer id_causal) throws Exception {
         if(id_causal ==0){
            throw new Exception("El id es obligatorio");
        }
        return causalDAO.findById(id_causal);
    }

    @Override
    public List<Causal> findAll() throws Exception {
        return causalDAO.findAll();
    }
    
    
}
