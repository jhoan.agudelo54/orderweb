/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Observation;
import co.edu.sena.orderweb.persistence.IObservationDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class ObservationBean implements ObservationBeanLocal {

    @EJB
    private IObservationDAO observationDAO;

    public void validate(Observation observation) throws Exception {
        if (observation == null) {
            throw new Exception("La causal es nula");
        }
        if (observation.getIdObservation() == 0) {
            throw new Exception("El id es obligatorio");
        }
        if (observation.getDescription().isEmpty()) {
            throw new Exception("La descripcion es obligatoria");

        }
    }

    @Override
    public void insert(Observation observation) throws Exception {
        validate(observation);
        Observation oldObservation = observationDAO.findById(observation.getIdObservation());
        if (oldObservation != null) {
            throw new Exception("Ya existe una observacion con el mismo ID");
        }

        observationDAO.insert(observation);
    }

    @Override
    public void update(Observation observation) throws Exception {
        validate(observation);
        Observation oldObservation = observationDAO.findById(observation.getIdObservation());
        if (oldObservation == null) {
            throw new Exception("No existe una observacion con el mismo ID");
        }
        oldObservation.setDescription(observation.getDescription());
        observationDAO.update(observation);
    }

    @Override
    public void delete(Observation observation) throws Exception {
        validate(observation);
        Observation oldObservation = observationDAO.findById(observation.getIdObservation());
        if (oldObservation == null) {
            throw new Exception("No existe una observacion con el mismo ID");
        }
        oldObservation.setDescription(observation.getDescription());
        observationDAO.delete(observation);
    }

    @Override
    public Observation findById(Integer id_observation) throws Exception {
         if(id_observation ==0){
            throw new Exception("El id es obligatorio");
        }
        return observationDAO.findById(id_observation);
    }

    @Override
    public List<Observation> findAll() throws Exception {
         return observationDAO.findAll();
    }

}
