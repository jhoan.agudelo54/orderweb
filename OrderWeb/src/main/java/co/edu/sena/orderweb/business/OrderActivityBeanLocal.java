/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.OrderActivity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jsam4
 */
@Local
public interface OrderActivityBeanLocal {
   public void insert(OrderActivity orderActivity) throws Exception;
    public void update(OrderActivity orderActivity) throws Exception;
    public void delete(OrderActivity orderActivity) throws Exception;
    public OrderActivity findById(Integer id_order,Integer id_activity) throws Exception;
    public List<OrderActivity> findAll() throws Exception; 
}
