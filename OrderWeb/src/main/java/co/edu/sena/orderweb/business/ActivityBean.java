/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Activity;
import co.edu.sena.orderweb.persistence.IActivityDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class ActivityBean implements ActivityBeanLocal{
    @EJB
     private IActivityDAO activityDAO;
    
     public void validate(Activity activity) throws Exception {
        if (activity == null) {
            throw new Exception("El tipo de actividad es nula");
        }
        if (activity.getIdActivity() == 0) {
            throw new Exception("El id es obligatorio");
        }
        if (activity.getHours() == 0) {
            throw new Exception("La hora es obligatoria");
        }
        if (activity.getDate()==null) {
            throw new Exception("La hora es obligatoria");
        }
        
     }
    @Override
    public void insert(Activity activity) throws Exception {
        validate(activity);
        activityDAO.insert(activity);
    }

    @Override
    public void update(Activity activity) throws Exception {
        validate(activity);
        Activity oldActivity = activityDAO.findById(activity.getIdActivity());
        if (oldActivity == null) {
            throw new Exception("No existe una actividad con el mismo ID");
        }
        oldActivity.setDescription(activity.getDescription());
        oldActivity.setHours(activity.getHours());
        oldActivity.setDate(activity.getDate());
        oldActivity.setIdTechnician(activity.getIdTechnician());
        oldActivity.setIdType(activity.getIdType());
        
        
        activityDAO.update(activity);
    }

    @Override
    public void delete(Activity activity) throws Exception {
        validate(activity);
        Activity oldActivity = activityDAO.findById(activity.getIdActivity());
        if (oldActivity == null) {
            throw new Exception("No existe una actividad con el mismo ID");
        }
        activityDAO.delete(oldActivity);
    }

    @Override
    public Activity findById(Integer id_activity) throws Exception {
         if (id_activity == 0) {
            throw new Exception("El id es obligatorio");
        }
        return activityDAO.findById(id_activity);
    }

    @Override
    public List<Activity> findAll() throws Exception {
        return activityDAO.findAll();
    }
    
}
