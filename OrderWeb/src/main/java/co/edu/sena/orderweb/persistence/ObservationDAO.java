/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.Observation;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ObservationDAO implements IObservationDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Observation observation) throws Exception {
        try {
           entityManager.persist(observation);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Observation observation) throws Exception {
        try {
           entityManager.merge(observation);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Observation observation) throws Exception {
         try {
           entityManager.remove(observation);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Observation findById(Integer id_observation ) throws Exception {
        
        try {
           return entityManager.find(Observation.class, id_observation);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Observation> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Observation.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
