/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.Order1;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class OrderDAO implements IOrderDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Order1 order) throws Exception {
        try {
           entityManager.persist(order);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Order1 order) throws Exception {
        try {
           entityManager.merge(order);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Order1 order) throws Exception {
         try {
           entityManager.remove(order);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Order1 findById(Integer id_order ) throws Exception {
        
        try {
           return entityManager.find(Order1.class, id_order);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Order1> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Order.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
