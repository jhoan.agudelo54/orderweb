/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.TypeActivity;
import co.edu.sena.orderweb.persistence.ITypeActivityDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class TypeActivityBean implements TypeActivityBeanLocal {

    @EJB
    private ITypeActivityDAO typeActivityDAO;

    public void validate(TypeActivity typeActivity) throws Exception {
        if (typeActivity == null) {
            throw new Exception("El tipo de actividad es nula");
        }
        if (typeActivity.getIdType() == 0) {
            throw new Exception("El id es obligatorio");
        }
        if (typeActivity.getDescription().isEmpty()) {
            throw new Exception("La descripcion es obligatoria");

        }
    }

    @Override
    public void insert(TypeActivity typeActivity) throws Exception {
        validate(typeActivity);
        TypeActivity oldTypeActivity = typeActivityDAO.findById(typeActivity.getIdType());
        if (oldTypeActivity != null) {
            throw new Exception("Ya existe un tipo de actividad con el mismo ID");
        }

        typeActivityDAO.insert(typeActivity);
    }

    @Override
    public void update(TypeActivity typeActivity) throws Exception {
        validate(typeActivity);
        TypeActivity oldTypeActivity = typeActivityDAO.findById(typeActivity.getIdType());
        if (oldTypeActivity == null) {
            throw new Exception("No existe un tipo de actividad con el mismo ID");
        }
        oldTypeActivity.setDescription(typeActivity.getDescription());
        typeActivityDAO.update(typeActivity);
    }

    @Override
    public void delete(TypeActivity typeActivity) throws Exception {
        validate(typeActivity);
        TypeActivity oldTypeActivity = typeActivityDAO.findById(typeActivity.getIdType());
        if (oldTypeActivity == null) {
            throw new Exception("No existe un tipo de actividad con el mismo ID");
        }

        typeActivityDAO.delete(oldTypeActivity);
    }

    @Override
    public TypeActivity findById(Integer id_type) throws Exception {
        if (id_type == 0) {
            throw new Exception("El id es obligatorio");
        }
        return typeActivityDAO.findById(id_type);
    }

    @Override
    public List<TypeActivity> findAll() throws Exception {
        return typeActivityDAO.findAll();
    }

}
