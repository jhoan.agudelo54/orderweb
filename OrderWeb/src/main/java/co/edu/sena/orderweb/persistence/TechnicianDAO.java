/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Technician;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class TechnicianDAO implements ITechnicianDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Technician technician) throws Exception {
        try {
           entityManager.persist(technician);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Technician technician) throws Exception {
        try {
           entityManager.merge(technician);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Technician technician) throws Exception {
         try {
           entityManager.remove(technician);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }


    @Override
    public List<Technician> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Technician.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }

    
    @Override
    public Technician findById(Long document) throws Exception {
         try {
           return entityManager.find(Technician.class, document);
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}

