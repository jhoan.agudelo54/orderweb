/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.TypeActivity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
public class TyperActivityDAO implements ITypeActivityDAO{

    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(TypeActivity typeActivity) throws Exception {
        try {
           entityManager.persist(typeActivity);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(TypeActivity typeActivity) throws Exception {
        try {
           entityManager.merge(typeActivity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(TypeActivity typeActivity) throws Exception {
         try {
           entityManager.remove(typeActivity);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public TypeActivity findById(Integer id_type ) throws Exception {
        
        try {
           return entityManager.find(TypeActivity.class, id_type);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<TypeActivity> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("TypeActivity.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
