/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IUserDAO {
   
    public User findById(String username) throws Exception;
    public List<User> findAll() throws Exception;
}
