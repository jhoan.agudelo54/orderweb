/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Activity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jsam4
 */
        @Local
public interface ActivityBeanLocal {
    public void insert(Activity activity) throws Exception;
    public void update(Activity activity) throws Exception;
    public void delete(Activity activity) throws Exception;
    public Activity findById(Integer id_activity) throws Exception;
    public List<Activity> findAll() throws Exception;
}

