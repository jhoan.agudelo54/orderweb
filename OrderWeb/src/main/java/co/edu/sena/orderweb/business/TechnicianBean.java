/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Technician;
import co.edu.sena.orderweb.persistence.ITechnicianDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class TechnicianBean implements TechnicianBeanLocal {

    @EJB
    private ITechnicianDAO technicianDAO;

    public void validate(Technician technician) throws Exception {
        if (technician == null) {
            throw new Exception("El tipo de actividad es nula");
        }
        if (technician.getDocument() == 0) {
            throw new Exception("El documento es obligatorio");
        }
        if (technician.getName().isEmpty()) {
            throw new Exception("La nombre es obligatorio");
        }
        if (technician.getEspeciality().isEmpty()) {
            throw new Exception("La especialidad es obligatoria");
        }
        if (technician.getPhone().isEmpty()) {
            throw new Exception("La telefono es obligatorio");
        }
    }

    @Override
    public void insert(Technician technician) throws Exception {
        validate(technician);
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if (oldTechnician != null) {
            throw new Exception("Ya existe un tecnico con el mismo Documento");
        }

        technicianDAO.insert(technician);
    }

    @Override
    public void update(Technician technician) throws Exception {
        validate(technician);
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if (oldTechnician == null) {
            throw new Exception("No existe un tecnico con el mismo Documento");
        }
        oldTechnician.setEspeciality(technician.getEspeciality());
        oldTechnician.setName(technician.getName());
        oldTechnician.setPhone(technician.getPhone());
        technicianDAO.update(technician);
    }

    @Override
    public void delete(Technician technician) throws Exception {
        validate(technician);
        Technician oldTechnician = technicianDAO.findById(technician.getDocument());
        if (oldTechnician == null) {
            throw new Exception("No existe un tecnico con el mismo Documento");
        }
        technicianDAO.delete(technician);
    }

    @Override
    public List<Technician> findAll() throws Exception {
        return technicianDAO.findAll();
    }

    @Override
    public Technician findById(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("El id es obligatorio");
        }
        return technicianDAO.findById(document);
    }

   
}
