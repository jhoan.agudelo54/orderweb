/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.TypeActivity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author jsam4
 */
@Local
public interface TypeActivityBeanLocal {
    public void insert(TypeActivity typeActivity) throws Exception;
    public void update(TypeActivity typeActivity) throws Exception;
    public void delete(TypeActivity typeActivity) throws Exception;
    public TypeActivity findById(Integer id_type) throws Exception;
    public List<TypeActivity> findAll() throws Exception;
}
