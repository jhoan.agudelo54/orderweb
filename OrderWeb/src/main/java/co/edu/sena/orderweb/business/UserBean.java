/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.User;
import co.edu.sena.orderweb.persistence.IUserDAO;
import java.util.List;
import javax.ejb.EJB;


/**
 *
 * @author jsam4
 */
public class UserBean implements UserBeanLocal {

     @EJB
    private IUserDAO userDAO;
    
    @Override
    public User findById(String username) throws Exception {
        if(username.isEmpty()){
            throw new Exception("El Nombre es obligatorio");
        }
        return userDAO.findById(username);
    }

    @Override
    public List<User> findAll() throws Exception {
        return userDAO.findAll();
    }
    
     public String encryptPassword(String password){
       // String encryptMd5 = DigestUtils.md5Hex(password);
        return password;
    }
    
    @Override
    public void login(User user) throws Exception {
        
        if(user==null){
            throw new Exception("El usuario es nullo");
        }
        if(user.getUsername().isEmpty()){
            throw new Exception("El nombre es obligatorio");
        }
        //consulto si existe un usuario con el nombre ingresado en el login
        User oldUser = userDAO.findById(user.getUsername());
        if(oldUser==null){
            throw new Exception("usuario incorrecto!");
        }
        //se encripta la contraseña digitada en login para comparla en la bd 
        String passwordEncriptado = encryptPassword(user.getPassword());
        if(!oldUser.getPassword().equals(passwordEncriptado)){
            throw new Exception("usuario incorrecto!");
        } 
        
    }
    
}
