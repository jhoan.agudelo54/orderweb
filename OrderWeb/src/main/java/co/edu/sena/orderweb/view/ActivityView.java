/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.ActivityBeanLocal;
import co.edu.sena.orderweb.business.TechnicianBeanLocal;
import co.edu.sena.orderweb.business.TypeActivityBeanLocal;
import co.edu.sena.orderweb.model.Activity;
import co.edu.sena.orderweb.model.Technician;
import co.edu.sena.orderweb.model.TypeActivity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 * objetivo: administrar la vista de Activity
 *
 * @author Aprendiz
 */
@Named(value = "activityView")
@RequestScoped
public class ActivityView {

    private InputText txtIdActivity;
    private InputText txtDescription;
    private InputText txtHours;
    private Date Date;

    private SelectOneMenu selectIdTechnician;
    private SelectOneMenu selectIdType;

    private CommandButton btnModificar;
    private CommandButton btnCrear;
    private CommandButton btnEliminar;

    private List<Activity> listActivity = null;

    private List<SelectItem> itemsTechnician;
    private List<SelectItem> itemsTypeActivity;
    @EJB
    private ActivityBeanLocal activityBean;
    @EJB
    private TechnicianBeanLocal technicianBean;
    @EJB
    private TypeActivityBeanLocal typeActivityBean;

    /**
     * Creates a new instance of ActivityView
     */
    public ActivityView() {

    }

    public InputText getTxtIdActivity() {
        return txtIdActivity;
    }

    public void setTxtIdActivity(InputText txtIdActivity) {
        this.txtIdActivity = txtIdActivity;
    }

    public InputText getTxtDescription() {
        return txtDescription;
    }

    public void setTxtDescription(InputText txtDescription) {
        this.txtDescription = txtDescription;
    }

    public InputText getTxtHours() {
        return txtHours;
    }

    public void setTxtHours(InputText txtHours) {
        this.txtHours = txtHours;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public SelectOneMenu getSelectIdTechnician() {
        return selectIdTechnician;
    }

    public void setSelectIdTechnician(SelectOneMenu selectIdTechnician) {
        this.selectIdTechnician = selectIdTechnician;
    }

    public SelectOneMenu getSelectIdType() {
        return selectIdType;
    }

    public void setSelectIdType(SelectOneMenu selectIdType) {
        this.selectIdType = selectIdType;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<Activity> getListActivity() {

        if (listActivity == null) {
            try {
                listActivity = activityBean.findAll();
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
        return listActivity;
    }

    public void setListActivity(List<Activity> listActivity) {
        this.listActivity = listActivity;
    }

    public List<SelectItem> getItemsTechnician() {

        try {
            List<Technician> listTechnician = technicianBean.findAll();
            itemsTechnician = new ArrayList<>();
            for (Technician technician : listTechnician) {
                itemsTechnician.add(new SelectItem(technician.getDocument()));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
        return itemsTechnician;
    }

    public void setItemsTechnician(List<SelectItem> itemsTechnician) {
        this.itemsTechnician = itemsTechnician;
    }

    public List<SelectItem> getItemsTypeActivity() {
        try {
            List<TypeActivity> listTypeActivity = typeActivityBean.findAll();
            itemsTypeActivity = new ArrayList<>();
            for (TypeActivity typeActivity : listTypeActivity) {
                itemsTechnician.add(new SelectItem(typeActivity.getIdType()));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
        return itemsTypeActivity;
    }

    public void setItemsTypeActivity(List<SelectItem> itemsTypeActivity) {
        this.itemsTypeActivity = itemsTypeActivity;
    }

    public void clear() {

        txtIdActivity.setValue("");
        txtDescription.setValue("");
        txtHours.setValue("");
        Date = null;

        selectIdTechnician.setValue("");
        selectIdType.setValue("");

        listActivity = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            Activity activity = new Activity();

            activity.setIdActivity(Integer.parseInt(txtIdActivity.getValue().toString()));
            activity.setDescription(txtDescription.getValue().toString());
            activity.setHours(Integer.parseInt(txtHours.getValue().toString()));
            activity.setDate(Date);

            //FK Technician
            Technician technician = technicianBean.findById(Long.parseLong(selectIdTechnician.getValue().toString()));
            activity.setIdTechnician(technician);

            //FK TypeActivity
            TypeActivity typeActivity = typeActivityBean.findById(Integer.parseInt(selectIdType.getValue().toString()));
            activity.setIdType(typeActivity);
            activityBean.insert(activity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Activity creado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void update() {
        try {
            Activity activity = new Activity();

            activity.setIdActivity(Integer.parseInt(txtIdActivity.getValue().toString()));
            activity.setDescription(txtDescription.getValue().toString());
            activity.setHours(Integer.parseInt(txtHours.getValue().toString()));
            activity.setDate(Date);

            //FK Technician
            Technician technician = technicianBean.findById(Long.parseLong(selectIdTechnician.getValue().toString()));
            activity.setIdTechnician(technician);

            //FK TypeActivity
            TypeActivity typeActivity = typeActivityBean.findById(Integer.parseInt(selectIdType.getValue().toString()));
            activity.setIdType(typeActivity);
            activityBean.update(activity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Activity creado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void delete() {
        try {
            Activity activity = new Activity();
            activity.setIdActivity(Integer.parseInt(txtIdActivity.getValue().toString()));
            activityBean.delete(activity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Acitivity eliminado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void onRowSelect(SelectEvent event) {

        Activity activity = (Activity) event.getObject();
        txtIdActivity.setValue(activity.getIdActivity());
        txtDescription.setValue(activity.getDescription());
        txtHours.setValue(activity.getHours());
        Date = activity.getDate();
        

        //FK
        selectIdTechnician.setValue(activity.getIdTechnician().getDocument());
        selectIdType.setValue(activity.getIdType().getIdType());

        //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            Activity activity = activityBean.findById(Integer.parseInt(txtIdActivity.getValue().toString()));

            if (activity != null) {
                
                txtDescription.setValue(activity.getDescription());
                txtHours.setValue(activity.getHours());
                activity.getDate();   

                //FK
                selectIdTechnician.setValue(activity.getIdTechnician().getDocument());
                selectIdType.setValue(activity.getIdType().getIdType());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                txtDescription.setValue("");
                txtHours.setValue("");
                Date =null;  
                
                listActivity = null;    
                
                selectIdTechnician.setValue("");
                selectIdType.setValue("");                

                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Activity no registrado"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

}
