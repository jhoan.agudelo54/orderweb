/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Order1;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IOrderDAO {
    public void insert(Order1 order1) throws Exception;
    public void update(Order1 order1) throws Exception;
    public void delete(Order1 order1) throws Exception;
    public Order1 findById(Integer id_order) throws Exception;
    public List<Order1> findAll() throws Exception;
}
