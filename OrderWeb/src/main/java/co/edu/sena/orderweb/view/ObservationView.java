/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.ObservationBeanLocal;
import co.edu.sena.orderweb.model.Observation;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author usuario
 */
@Named(value = "observationView")
@RequestScoped
public class ObservationView {
    
    private InputText txtIdObservation;
    private InputText txtDescription;
    
    private CommandButton btnModificar;
    private CommandButton btnCrear;
    private CommandButton btnEliminar;
    
    private List<Observation> listObservation = null;
    
    @EJB
    private ObservationBeanLocal observationBean;
    /**
     * Creates a new instance of ObservationView
     */
    public ObservationView() {
    }

    public InputText getTxtIdObservation() {
        return txtIdObservation;
    }

    public void setTxtIdObservation(InputText txtIdObservation) {
        this.txtIdObservation = txtIdObservation;
    }

    public InputText getTxtDescription() {
        return txtDescription;
    }

    public void setTxtDescription(InputText txtDescription) {
        this.txtDescription = txtDescription;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<Observation> getListObservation() {
        if (listObservation == null) {
            try {
                listObservation = observationBean.findAll();
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
        return listObservation;
    }

    public void setListObservation(List<Observation> listObservation) {
        this.listObservation = listObservation;
    }
    public void clear() {

        txtIdObservation.setValue("");
        txtDescription.setValue("");        

        listObservation = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            Observation observation = new Observation();

            
            observation.setDescription(txtDescription.getValue().toString());
            
            observationBean.insert(observation);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Observation creado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void update() {
        try {
            Observation causal = new Observation();

            causal.setIdObservation(Integer.parseInt(txtIdObservation.getValue().toString()));
            causal.setDescription(txtDescription.getValue().toString());
            
            observationBean.update(causal);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Observation modificado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void delete() {
        try {
            Observation observation = new Observation();
            observation.setIdObservation(Integer.parseInt(txtIdObservation.getValue().toString()));
            observationBean.delete(observation);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Observation eliminado exitosamente"));
            clear();

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void onRowSelect(SelectEvent event) {

        Observation observation = (Observation) event.getObject();
        txtIdObservation.setValue(observation.getIdObservation());
        txtDescription.setValue(observation.getDescription());                
        
        //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            Observation observation = observationBean.findById(Integer.parseInt(txtIdObservation.getValue().toString()));

            if (observation != null) {
                
                txtDescription.setValue(observation.getDescription());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                txtDescription.setValue("");
                
                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Observation no registrado"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }
    
}
