/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Order1;
import co.edu.sena.orderweb.persistence.IOrderDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author jsam4
 */
@Stateless
public class OrderBean implements OrderBeanLocal {

    @EJB
    private IOrderDAO orderDAO;

    public void validate(Order1 order1) throws Exception {
        if (order1 == null) {
            throw new Exception("El tipo de actividad es nula");
        }
        if (order1.getLegalizationDate() == null) {
            throw new Exception("El id es obligatorio");
        }
        if (order1.getAddress().isEmpty()) {
            throw new Exception("La direccion es obligatoria");
        }
        if (order1.getCity().isEmpty()) {
            throw new Exception("La ciudad es obligatoria");
        }
        if (order1.getState().isEmpty()) {
            throw new Exception("La estado es obligatoria");
        }
    }

    @Override
    public void insert(Order1 order1) throws Exception {
        validate(order1);
        orderDAO.insert(order1);
    }

    @Override
    public void update(Order1 order1) throws Exception {
        validate(order1);
        Order1 oldOrder1 = orderDAO.findById(order1.getIdOrder());
        if (oldOrder1 == null) {
            throw new Exception("No existe una actividad con el mismo ID");
        }
        oldOrder1.setLegalizationDate(order1.getLegalizationDate());
        oldOrder1.setAddress(order1.getAddress());
        oldOrder1.setCity(order1.getCity());
        oldOrder1.setState(order1.getState());
        oldOrder1.setIdObservation(order1.getIdObservation());
        oldOrder1.setIdCausal(order1.getIdCausal());
  
        orderDAO.update(order1);
    }

    @Override
    public void delete(Order1 order1) throws Exception {
        validate(order1);
        Order1 oldOrder1 = orderDAO.findById(order1.getIdOrder());
        if (oldOrder1 == null) {
            throw new Exception("No existe una actividad con el mismo ID");
        }
        
        orderDAO.delete(order1);
    }

    @Override
    public Order1 findById(Integer id_order) throws Exception {
        if (id_order == 0) {
            throw new Exception("El id es obligatorio");
        }
        return orderDAO.findById(id_order);
    }

    @Override
    public List<Order1> findAll() throws Exception {
        return orderDAO.findAll();
    }

}
